const monthes = [
    'Січня',
    'Лютого',
    'Березня',
    'Квітня',
    'Травня',
    'Червня',
    'Липня',
    'Серпня',
    'Вересня',
    'Жовтня',
    'Листопада',
    'Грудня',
]
function task1_btn_handler(){
    const result_field = document.querySelector("#current_date")
    let today = new Date()
    result_field.innerHTML = `
        Дата: ${today.getDate()} ${monthes[today.getMonth()]} ${today.getFullYear()}
        <br/>
        День: ${today.toLocaleDateString('uk-Uk', {weekday: 'long'})}
        <br/>
        Час: ${today.toLocaleTimeString()}
    `
}

function task2_btn_handler(date = new Date()){
    const result_field = document.querySelector("#day_of_weak")
    result_field.innerHTML = `
        Номер дня: ${date.getDate()}
        <br/>
        Назва дня: ${date.toLocaleDateString('uk-Uk', {weekday: 'long'})}
    `
    return {
        dayNumber: date.getDate(),
        dayName: date.toLocaleDateString('uk-Uk', {weekday: 'long'})
    }
}

function task3_btn_handler(){
    const result_field = document.querySelector("#finding_date")
    const day_count_field = document.querySelector("#day_count")
    const dayMilliseconds = (24*day_count_field.value)*60*60*1000;
    const result = new Date(new Date().setTime(new Date().getTime() - dayMilliseconds))

    result_field.innerHTML = `
        Дата: ${result.getDate()} ${monthes[result.getMonth()]} ${result.getFullYear()}
        <br/>
        День: ${result.toLocaleDateString('uk-Uk', {weekday: 'long'})}
    `
}
const getMonthDayCount = (year, month) => { return new Date(year, month, 0).getDate(); };
function task4_btn_handler(){
    const result_field = document.querySelector("#last_day")
    const last_day_year_field = document.querySelector("#last_day_year")
    const last_day_month_field = document.querySelector("#last_day_month")
    const calculated_day = new Date(last_day_year_field.value, last_day_month_field.value-1, getMonthDayCount(last_day_year_field.value, last_day_month_field.value))

    result_field.innerHTML = `
        Дата: ${calculated_day.getDate()} ${monthes[calculated_day.getMonth()]} ${calculated_day.getFullYear()}
        <br/>
        День: ${calculated_day.toLocaleDateString('uk-Uk', {weekday: 'long'})}
    `
}

function task5_btn_handler(){
    function getSecondsToday() {
        let now = new Date();
        let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        let diff = now - today;
        return Math.round(diff / 1000);
    }

    const result_field = document.querySelector("#seconds_count")
    const seconds_from_start = getSecondsToday()
    const seconds_to_end = 86400 - getSecondsToday()

    result_field.innerHTML = `
        Секунд від початку дня: ${seconds_from_start}
        <br/>
        Секунд до кінця дня: ${seconds_to_end}
    `
}

function Grinvich(first_date, second_date){
    return 720 - Number((second_date.getHours() * 60 + second_date.getMinutes()) -
        (first_date.getHours() * 60 + first_date.getMinutes()));
}
// console.log(Grinvich(new Date(0,0,0,5,30),
//     new Date(0,0,0,18,20)));